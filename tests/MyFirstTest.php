<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 04/11/17
 * Time: 20:45
 */
use SON\Area;

class MyFirstTest extends PHPUnit\Framework\TestCase
{
    public function testArray()
    {
        $array = ['teste'];
        $this->assertNotEmpty($array);
    }

    public function testArea()
    {
        $area = new Area();
        $this->assertEquals(6, $area->getArea(2,3));
    }
}