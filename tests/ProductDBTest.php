<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 05/11/17
 * Time: 00:04
 */
use PHPUnit\Framework\TestCase;

class ProductDBTest extends TestCase
{
    protected $backupGlobals = false;

    public function testIfProductIsSaved()
    {
        global $db;
        $product = new \SON\Model\Product($db);
        $result = $product->save([
            'name' => 'Produto 1',
            'price' => 200.20,
            'quantity' => 10
        ]);

        $this->assertEquals(1, $result->getId());
        $this->assertEquals('Produto 1', $result->getName());
        $this->assertEquals(200.20, $result->getPrice());
        $this->assertEquals(10, $result->getQuantity());
        $this->assertEquals(200.20 * 10, $result->getTotal());
        return $result->getId();
    }

    public function testIfListProducts()
    {
        global $db;
        $product = new \SON\Model\Product($db);
        $result = $product->save([
            'name' => 'Produto 1',
            'price' => 200.20,
            'quantity' => 10
        ]);

        $products = $product->all();
        $this->assertCount(2, $products);
    }

    /**
     * @depends testIfProductIsSaved
     */
    public function testIfProductIsUpdate($id)
    {
        global $db;
        $product = new \SON\Model\Product($db);
        $result = $product->save([
            'id' => $id,
            'name' => 'Produto Alt',
            'price' => 300.20,
            'quantity' => 20
        ]);

        $this->assertEquals(1, $result->getId());
        $this->assertEquals('Produto Alt', $result->getName());
        $this->assertEquals(300.20, $result->getPrice());
        $this->assertEquals(20, $result->getQuantity());
        $this->assertEquals(300.20 * 20, $result->getTotal());
        return $id;
    }

    /**
     * @depends testIfProductIsUpdate
     */
    public function testIfProductCanBeRecovered($id)
    {
        global $db;
        $product = new \SON\Model\Product($db);
        $result = $product->find($id);

        $this->assertEquals(1, $result->getId());
        $this->assertEquals('Produto Alt', $result->getName());
        $this->assertEquals(300.20, $result->getPrice());
        $this->assertEquals(20, $result->getQuantity());
        $this->assertEquals(300.20 * 20, $result->getTotal());
    }

    /**
     * @depends testIfProductIsUpdate
     */
    public function testIfProductCanDeleted($id)
    {
        global $db;
        $product = new \SON\Model\Product($db);
        $result = $product->delete($id);

        $this->assertTrue($result);
        $products = $product->all();
        $this->assertCount(1, $products);

    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Produto não existente
     */
    public function testIfProductNotFound()
    {
        global $db;
        $product = new \SON\Model\Product($db);
        $product->find(999999);

    }
}