<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 04/11/17
 * Time: 22:17
 */

use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    private $product;

    protected function setUp()
    {
        $pdo = $this->getMockBuilder(\PDO::class)
            ->disableOriginalConstructor()->getMock();
        $this->product = new \SON\Model\Product($pdo);
    }
    //Não se aplica
    /*public function testIfIdIsNull()
    {
        $product = new \SON\Model\Product();

        $this->assertNull($product->getId());
    }*/

    public function testSetAndGetName()
    {
        $this->assertInstanceOf(\SON\Model\Product::class, $this->product->setName("Meu produto 1"));
        $this->assertEquals("Meu produto 1", $this->product->getName());

    }

    public function testSetAndGetPrice()
    {
        $this->assertInstanceOf(\SON\Model\Product::class, $this->product->setPrice(10.10));
        $this->assertEquals(10.10, $this->product->getPrice());
    }

    public function testSetAndGetQuantity()
    {
        $this->assertInstanceOf(\SON\Model\Product::class, $this->product->setQuantity(5));
        $this->assertEquals(5, $this->product->getQuantity());
    }

    //Não se aplica
    /*public function testIfTotalIsNull()
    {
        $product = new \SON\Model\Product();

        $this->assertNull($product->getTotal());
    }*/

    /**
     * @dataProvider collectionData
     */
    public function testEncapsulate($property, $expected, $actual)
    {
        $set = $this->product->{'set'.ucfirst($property)}($actual);
        $this->assertInstanceOf(\SON\Model\Product::class, $set);
        $get = $this->product->{'get'.ucfirst($property)}();
        $this->assertEquals($expected, $get);
    }

    public function collectionData()
    {
        return [
            ['name','Produto 1', 'Produto 1'],
            ['price',10.11, 10.11],
            ['quantity',5, 5],
        ];
    }
}