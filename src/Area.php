<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 04/11/17
 * Time: 21:11
 */
namespace SON;

class Area
{
    public function getArea($valor1, $valor2)
    {
        return $valor1 * $valor2;
    }
}